<!-- Footer START -->
<footer id="footer">
	<div class="container">
		<div class="col-md-3 col-sm-6 col-xs-12">
			<h2>About us</h2>
			<p>I am text block. Click edit button to change this text. Lorem ipsum dolor sit amet, consectetur. I am text block. Click edit button to change this text. </p>
		</div>

		<div class="col-md-3 col-sm-6 col-xs-12">
			<h2>Opening Hours</h2>
			<ul>
				<li>Mon-Thu: 8:00 - 17:00</li>
				<li>Fri: 8:00 - 19:00</li>
				<li>Sat: 11:00 - 14:00</li>
				<li>Sun: Closed</li>
				<li>Sat: Closed</li>
			</ul>
		</div>

		<div class="col-md-3 col-sm-6 col-xs-12">
			<h2>Pages</h2>
			<ul>
				<li><a href="#">Home</a></li>
				<li><a href="#">About</a></li>
				<li><a href="#">Services</a></li>
				<li><a href="#">Projects</a></li>
				<li><a href="#">Contact Us</a></li>												
			</ul>			
		</div>

		<div class="col-md-3 col-sm-6 col-xs-12">
			<h2>Contact info</h2>
			<ul class="footer_icon">
				<li><i class="fa fa-map-marker"></i>New York, NY Sheram 113 254</li>
				<li><i class="fa fa-phone"></i>Phone: +1-23-456789</li>
				<li><i class="fa fa-envelope-o"></i>E-mail: example@mail.com</li>
			</ul>			
		</div>						
	</div>
</footer>
<div class="footer-bar">
	<div class="container">
		<div class="col-md-6 col-sm-12 col-xs-12">
			<span>© Copyright 2017 SpecThemes. All Rights reserved</span>
		</div>
		<div class="col-md-6 col-sm-12 col-xs-12 right-holder">
			<a href="#"><i class="fa fa-facebook"></i></a>
			<a href="#"><i class="fa fa-twitter"></i></a>
			<a href="#"><i class="fa fa-instagram"></i></a>
			<a href="#"><i class="fa fa-pinterest"></i></a>		
		</div>		
	</div>
</div>
<!-- Footer END -->




<!-- Scroll to top button Start -->
<a href="#" class="scroll-to-top"><i class="fa fa-angle-up" aria-hidden="true"></i></a>	
<!-- Scroll to top button End -->




<!-- Jquery -->
<script src="<?php echo base_url()?>assets/js/jquery.min.js"></script>

<!-- Bootstrap JS-->
<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>	

<!-- Owl Carousel-->
<script src="<?php echo base_url()?>assets/js/owl.carousel.js"></script>

<!-- Wow JS -->
<script src="<?php echo base_url()?>assets/js/wow.min.js"></script>

<!-- Countup -->
<script src="<?php echo base_url()?>assets/js/jquery.counterup.min.js"></script>
<script src="<?php echo base_url()?>assets/js/waypoints.min.js"></script>

<!-- Isotop -->
<script src="<?php echo base_url()?>assets/js/isotope.pkgd.min.js"></script>

<!-- Modernizr -->
<script src="<?php echo base_url()?>assets/js/modernizr.js"></script>

<script src="<?php echo base_url()?>assets/js/revolution/jquery.themepunch.tools.min.js"></script>
<script src="<?php echo base_url()?>assets/js/revolution/jquery.themepunch.revolution.min.js"></script>
<script src='<?php echo base_url()?>assets/js/revolution/revolution.addon.slicey.min.js?ver=1.0.0'></script>
<script src="<?php echo base_url()?>assets/js/revolution/revolution.extension.actions.min.js"></script>
<script src="<?php echo base_url()?>assets/js/revolution/revolution.extension.kenburn.min.js"></script>
<script src="<?php echo base_url()?>assets/js/revolution/revolution.extension.layeranimation.min.js"></script>
<script src="<?php echo base_url()?>assets/js/revolution/revolution.extension.migration.min.js"></script>
<script src="<?php echo base_url()?>assets/js/revolution/revolution.extension.slideanims.min.js"></script>



<!-- Main JS -->
<script src="<?php echo base_url()?>assets/js/main.js"></script>

</body>
</html>