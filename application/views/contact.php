<?php  $this->load->view('header');

?>


<!-- Page Title START -->
<div class="page-title" style="background-image: url(<?php echo base_url();?>assets/img/logos/banner1.jpg); background-position: center;">
	<div class="container">
		<h1>Contact</h1>
		<ul>
			<li><a href="index.html">Home</a></li>
			<li><a href="contact.html">Contact</a></li>
		</ul>
	</div>
</div>
<!-- Page Title END -->





<!-- Contact START -->
<div class="section-block">
	<div class="container">		
		<div class="row">
			<div class="col-md-4 col-sx-4 col-xs-12">
				<div class="contact-box clearfix">
					<div class="contact-icon">
						<i class="icon-map"></i>
					</div>
					<div class="contact-info">
						<h4>Our Address</h4>
						<p>West road, London, England <br> Seram 113</p>
					</div>
				</div>				
			</div>

			<div class="col-md-4 col-sx-4 col-xs-12">
				<div class="contact-box clearfix">
					<div class="contact-icon">
						<i class="icon-smartphone2"></i>
					</div>
					<div class="contact-info">
						<h4>Phone & Email</h4>
						<p>(+123) 123456789 <br> example@gmail.com</p>

					</div>
				</div>				
			</div>

			<div class="col-md-4 col-sx-4 col-xs-12">
				<div class="contact-box clearfix">
					<div class="contact-icon">
						<i class="icon-clock"></i>
					</div>
					<div class="contact-info">
						<h4>Working Hours</h4>
						<p>Time: Tusday-Monday <br> 9:00am - 6:00pm</p>
					</div>
				</div>				
			</div>
		</div>
	</div>
</div>	


<div class="section-block-grey">
	<div class="container">
		<div class="section-heading center-holder">
			<h2>Let's Talk about your business</h2>
			<div class="heading-line"></div>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor<br> incididunt ut labore et dolore magna aliqua. </p>
		</div>			
		<div>
			<form method="post" class="contact-form">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="email" name="email" placeholder="E-mail adress">
					</div>

					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" name="phone" placeholder="Phone Number">
					</div>

					<div class="col-xs-12">
						<textarea name="message" placeholder="Your Message"></textarea>
					</div>

					<div class="center-holder">
						<button type="submit" class="button-xs dark-button mt-20 mb-15">Send Message</button>
					</div>
				</div>							
			</form>	
		</div>
	</div>		
</div>
<!-- Contact END -->


<!-- Map START -->
<div id="map">
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBk25E4mNfVIEt3tNl3K1HwNZVruVoFBlA&callback=initMap">
	</script>  	
</div>
<!-- Map START -->



<!-- Partners Section START -->
<div class="partner-section">
	<div class="container">	
        <div class="owl-carousel owl-theme partners" id="partners">
            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">  
            </div>	

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">
            </div>

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">  
            </div>

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">  
            </div>

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">  
            </div>

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image"> 
            </div>            
        </div>  		     	
	</div>
</div>
<!-- Partners Section END -->


<?php  $this->load->view('footer');

?>