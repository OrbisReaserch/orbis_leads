<?php  $this->load->view('header');

?>




<!-- Page Title START -->
<div class="page-title" style="background-image: url(<?php echo base_url();?>assets/img/logos/banner1.jpg); background-position: center;">
	<div class="container">
		<h1>About</h1>
		<ul>
			<li><a href="index.html">Home</a></li>
			<li><a href="about.html">About</a></li>
		</ul>
	</div>
</div>
<!-- Page Title END -->




<!-- About Section START -->
<div class="section-block">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<div class="video-box">
					<img src="http://via.placeholder.com/555x313" alt="about-img" class="rounded-border">
					<div class="video-box-overlay">
						<div class="video-box-button">					
							<button class="video-play-icon" data-toggle="modal" data-target=".video-modal">
								<i class="fa fa-play"></i>
							</button>
						</div>					
						<!-- Modal Start -->
						<div class="modal fade video-modal" id="videomodal" tabindex="-1" role="dialog">
						  <div class="modal-dialog modal-lg" role="document">
							<iframe height="415" src="https://www.youtube.com/embed/EWzsJG07vHY" class="full-width round-frame image-shadow" allowfullscreen=""></iframe>
						  </div>
						</div>	
						<!-- Modal End -->
					</div>				
				</div>	
			</div>
			<div class="col-md-5 col-sm-6 col-xs-12 col-md-offset-1">
				<div class="section-heading left-holder">
					<span>What we do?</span>
					<h2>We do things differently</h2>
				</div>
				<div class="text-content mt-30">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				</div>	
				<div class="mt-20">
					<a href="#" class="dark-button button-xs">Read More</a>
				</div>			
			</div>			
		</div>

		<div class="row mt-90">
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="article-box">
					<i class="icon-laptop"></i>
					<h3>Branding</h3>
					<p>Classic Business provides complete solutions for multi page website.No coding skills required.</p>
				</div>
			</div>

			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="article-box">
					<i class="icon-line-graph-12"></i>
					<h3>Development</h3>
					<p>Classic Business provides complete solutions for multi page website.No coding skills required.</p>
				</div>
			</div>

			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="article-box">
					<i class="icon-push-pin2"></i>
					<h3>Support</h3>
					<p>Classic Business provides complete solutions for multi page website.No coding skills required.</p>
				</div>
			</div>						
		</div>		
	</div>
</div>
<!-- About Section END -->




<!-- Team START -->
<div class="section-block-grey">
	<div class="container">
		<div class="section-heading center-holder">
			<h2>Meet Our Team</h2>
			<div class="heading-line"></div>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor<br> incididunt ut labore et dolore magna aliqua. </p>
		</div>			
		<div class="row mt-40">
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="team-box">
					<div class="team-box-img">
						 <img src="http://via.placeholder.com/360x460" alt="team-img">
					</div>
					<div class="team-social">
						<h4>Daniel Gipson</h4>
						<h5>Web Designer</h5>

						<div class="team-social-icom">
							<a href="#"><i class="fa fa-facebook"></i></a>
							<a href="#"><i class="fa fa-instagram"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-google-plus"></i></a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="team-box">
					<div class="team-box-img">
						 <img src="http://via.placeholder.com/360x460" alt="team-img">
					</div>
					<div class="team-social">
						<h4>Daniel Gipson</h4>
						<h5>Web Designer</h5>

						<div class="team-social-icom">
							<a href="#"><i class="fa fa-facebook"></i></a>
							<a href="#"><i class="fa fa-instagram"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-google-plus"></i></a>
						</div>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="team-box">
					<div class="team-box-img">
						 <img src="http://via.placeholder.com/360x460" alt="team-img">
					</div>
					<div class="team-social">
						<h4>Daniel Gipson</h4>
						<h5>Web Designer</h5>
						<div class="team-social-icom">
							<a href="#"><i class="fa fa-facebook"></i></a>
							<a href="#"><i class="fa fa-instagram"></i></a>
							<a href="#"><i class="fa fa-twitter"></i></a>
							<a href="#"><i class="fa fa-google-plus"></i></a>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</div>
</div>
<!-- Team END -->




<!-- Projects START -->
<div class="section-block">
	<div class="container">
		<div class="section-heading center-holder">
			<span>Proffesional</span>
			<h2>Our Portfolio</h2>
			<div class="heading-line"></div>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod<br> temporincididunt ut labore et dolore magna aliqua.</p>
		</div>		
	    <div id="filters" class="center-holder mt-40"> 
	      <button class="isotop-button" data-filter="*">Show all</button>
	      <button class="isotop-button" data-filter=".financical">Financical</button>
	      <button class="isotop-button" data-filter=".business">Business</button>
	      <button class="isotop-button" data-filter=".marketing">Marketing</button>
	    </div>		
	    <div class="row">
			<div class="isotope-grid">
		        <div class="col-md-4 col-sm-6 col-xs-12 isotope-item business marketing">
		          <a href="#">
		            <div class="project-item">
		              <div class="overlay-container">
		                <img src="http://via.placeholder.com/360x250" alt="project-1">
		                <div class="project-item-overlay">
		                  <h4>Project 1</h4>
		                  <p>Lorem ipsum dolor sit amet</p>
		                </div>              
		              </div>              
		            </div>  
		          </a>        
		        </div>

		        <div class="col-md-4 col-sm-6 col-xs-12 isotope-item financical">
		          <a href="#">
		            <div class="project-item">
		              <div class="overlay-container">
		                <img src="http://via.placeholder.com/360x250" alt="project-1">
		                <div class="project-item-overlay">
		                  <h4>Project 1</h4>
		                  <p>Lorem ipsum dolor sit amet</p>
		                </div>              
		              </div>              
		            </div>  
		          </a>        
		        </div>

		        <div class="col-md-4 col-sm-6 col-xs-12 isotope-item financical marketing">
		          <a href="#">
		            <div class="project-item">
		              <div class="overlay-container">
		                <img src="http://via.placeholder.com/360x250" alt="project-1">
		                <div class="project-item-overlay">
		                  <h4>Project 1</h4>
		                  <p>Lorem ipsum dolor sit amet</p>
		                </div>              
		              </div>              
		            </div>  
		          </a>        
		        </div>

		        <div class="col-md-4 col-sm-6 col-xs-12 isotope-item marketing">
		          <a href="#">
		            <div class="project-item">
		              <div class="overlay-container">
		                <img src="http://via.placeholder.com/360x250" alt="project-1">
		                <div class="project-item-overlay">
		                  <h4>Project 1</h4>
		                  <p>Lorem ipsum dolor sit amet</p>
		                </div>              
		              </div>              
		            </div>  
		          </a>        
		        </div>	

		        <div class="col-md-4 col-sm-6 col-xs-12 isotope-item business">
		          <a href="#">
		            <div class="project-item">
		              <div class="overlay-container">
		                <img src="http://via.placeholder.com/360x250" alt="project-1">
		                <div class="project-item-overlay">
		                  <h4>Project 1</h4>
		                  <p>Lorem ipsum dolor sit amet</p>
		                </div>              
		              </div>              
		            </div>  
		          </a>        
		        </div>

		        <div class="col-md-4 col-sm-6 col-xs-12 isotope-item business">
		          <a href="#">
		            <div class="project-item">
		              <div class="overlay-container">
		                <img src="http://via.placeholder.com/360x250" alt="project-1">
		                <div class="project-item-overlay">
		                  <h4>Project 1</h4>
		                  <p>Lorem ipsum dolor sit amet</p>
		                </div>              
		              </div>              
		            </div>  
		          </a>        
		        </div>		        			        	        		        
        
			</div>
		</div>	
	</div>
</div>
<!-- Projects END -->





<!-- Parallax Section START -->
<div class="section-block-parallax" style="background-image: url(http://via.placeholder.com/1349x560);">	
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<img src="http://via.placeholder.com/536x419" alt="img">
			</div>
			<div class="col-md-5 col-sm-6 col-xs-12 col-md-offset-1">
				<div class="white-color section-heading left-holder mt-50">
					<h2>Camel clean & Unique HTML template design</h2>
					<div class="heading-line"></div>
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor<br> incididunt ut labore et dolore magna aliqua. </p>
				</div>				
				<a href="#" class="primary-button button-md mt-30">Purchuhase Now</a>
			</div>
		</div>
	</div>
</div>
<!-- Parallax Section END -->




<!-- Pricing START -->
<div class="section-block">
	<div class="container">
		<div class="section-heading center-holder">
			<span>Choose from best of three plans</span>
			<h2>Affordable Pricing</h2>
			<div class="heading-line"></div>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod<br> temporincididunt ut labore et dolore magna aliqua.</p>
		</div>	
		<div class="row mt-60">
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="pricing-block">
					<div class="pricing-block-top">
						<h4>Basic</h4>
						<h3>$9.99</h3>
						<p>Tutorials and Support</p>					
					</div>
					<ul>
						<li>10GB Storage</li>
						<li>10 Emails</li>
						<li>10 Domains</li>
						<li>1GB Bandwidth</li>
						<li>Email Support</li>
					</ul>
					<div class="center-holder">
						<a href="#">Sign Up</a>
					</div>				
				</div>
			</div>

			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="pricing-block">
					<div class="pricing-block-top">
						<h4>Pro</h4>
						<h3>$24.99</h3>
						<p>Tutorials and Support</p>					
					</div>
					<ul>
						<li>10GB Storage</li>
						<li>10 Emails</li>
						<li>10 Domains</li>
						<li>1GB Bandwidth</li>
						<li>Email Support</li>
					</ul>
					<div class="center-holder">
						<a href="#">Sign Up</a>
					</div>				
				</div>
			</div>

			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="pricing-block">
					<div class="pricing-block-top">
						<h4>Premium</h4>
						<h3>$49.99</h3>
						<p>Tutorials and Support</p>					
					</div>
					<ul>
						<li>10GB Storage</li>
						<li>10 Emails</li>
						<li>10 Domains</li>
						<li>1GB Bandwidth</li>
						<li>Email Support</li>
					</ul>
					<div class="center-holder">
						<a href="#">Sign Up</a>
					</div>				
				</div>
			</div>				
		</div>					
	</div>
</div>
<!-- Pricing END -->


<!-- Partners Section START -->
<div class="partner-section-grey">
	<div class="container">	
        <div class="owl-carousel owl-theme partners" id="partners">
            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">  
            </div>	

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">
            </div>

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">  
            </div>

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">  
            </div>

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">  
            </div>

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image"> 
            </div>            
        </div>  		     	
	</div>
</div>
<!-- Partners Section END -->



<?php  $this->load->view('footer');

?>