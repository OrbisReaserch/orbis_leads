<!DOCTYPE html>
<html lang="zxx">
<head>
	<title>Home</title>
	<meta charset="UTF-8">
	<link rel="shortcut icon" href="img/logos/logo-shortcut.png" />	
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Bootstrap CSS-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/bootstrap.min.css">
	
	<!-- Font-Awesome -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/font-awesome.css">  

	<!-- Icomoon -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/icomoon.css"> 
	
	<!-- Animate.css -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/animate.css">	

	<!-- Revolution Slider -->
	<link rel="stylesheet" href="<?php echo base_url()?>assets/css/rev-settings.css">	

	<!-- Owl Carousel  -->
    <link rel="stylesheet" href="<?php echo base_url()?>assets/css/owl.carousel.css">
	
	<!-- Main Styles -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/default.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/styles.css">

	<!-- Fonts Google -->
	<link href="https://fonts.googleapis.com/css?family=Work+Sans:100,200,300,400,500,600,700,800,900" rel="stylesheet">


</head>
<body>




<!-- Preloader Start-->
<div id="preloader">
	<div class="spinner">
	  <div class="bounce1"></div>
	  <div class="bounce2"></div>
	  <div class="bounce3"></div>
	</div>
</div>
<!-- Preloader End -->



<!-- Navbar START -->
<header>
	<nav class="navbar navbar-default navbar-custom navbar-fixed-top" data-offset-top="200">
	  <div class="container">
	  	<div class="row">
		    <div class="navbar-header navbar-header-custom">
		      <button type="button" class="navbar-toggle collapsed menu-icon" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-logo" href="<?php echo base_url()?>"><img src="<?php echo base_url();?>assets/img/logos/Final_Orbis Leads logo1.png" class=" logo_light" alt="logo"></a>
		      <a class="navbar-logo" href="<?php echo base_url()?>"><img src="<?php echo base_url();?>assets/img/logos/Final_Orbis Leads logo1.png" class="logo_main" alt="logo"></a>
		    </div>

		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="nav navbar-nav navbar-right navbar-links-custom">
		        <li><a href="<?php echo base_url();?>">Home</a></li>
		        <li >
		          <a href="<?php echo base_url();?>about" >About</a>
		       
		        </li>
		        <li >
		          <a href="<?php echo base_url();?>services" >Services</a>
		        
		        </li>				        			
		        <li class="dropdown" >
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Demand Generation</a>
		          <ul class="dropdown-menu dropdown-menu-left">
		            <li><a href="<?php echo base_url();?>demand_generation/account_based_marketing">Account based marketing</a></li>
		            <li><a href="<?php echo base_url();?>demand_generation/marketing_qualified_leads">Marketing qualified lead</a></li>
		            <!-- <li><a href="<?php echo base_url();?>demand_generation/content_marketing">Content marketing</a></li> -->
		            <li><a href="<?php echo base_url();?>demand_generation/event_promotion">Event promotion</a></li>
		          </ul>
		        </li>
		       <!--  <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pages</a>
		          <ul class="dropdown-menu dropdown-menu-left">
		            <li><a href="projects.php">Projects</a></li>
		            <li><a href="faq.php">FAQ</a></li>
		            <li><a href="404.php">404</a></li>
		          </ul>
		        </li>	 -->	        		        
		        <li><a href="<?php echo base_url();?>contact">Contact</a></li>
		      </ul>
		    </div>	
	  	</div>
	  </div>
	</nav>	
</header>
<!-- Navbar END -->




