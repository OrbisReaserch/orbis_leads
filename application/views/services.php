<?php  $this->load->view('header');

?>



<!-- Page Title START -->
<div class="page-title" style="background-image: url(<?php echo base_url();?>assets/img/logos/banner1.jpg); background-position: center;">
	<div class="container">
		<h1>Services</h1>
		<ul>
			<li><a href="index.html">Home</a></li>
			<li><a href="services.html">Services</a></li>
		</ul>
	</div>
</div>
<!-- Page Title END -->



<div class="section-block">
	<div class="container">
		<div class="row">
			<div class="col-md-6 col-sm-6 col-xs-12">
				<img src="http://via.placeholder.com/555x370" class="rounded-border" alt="img">
			</div>
			<div class="col-md-5 col-sm-6 col-xs-12 col-md-offset-1">
				<div class="section-heading left-holder">
					<span>Our mission</span>
					<h2>Effective solution for every businesses</h2>
				</div>
				<!-- Progress Bar Start -->
				<div class="progress-text mt-40">
					<div class="row">
				  	<div class="col-md-6 col-sm-6 col-xs-6">
				  		Mission Statement
				  	</div>
				  	<div class="col-md-6 col-sm-6 col-xs-6 text-right">
				  		45%
				  	</div>					  		
					</div>					  						  	
				</div>
				<div class="progress custom-progress">
					<div class="progress-bar primary-bar wow slideInLeft  animated" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width: 45%;">					      
					</div>
				</div>	
				<!-- Progress Bar End -->

				<!-- Progress Bar Start -->
				<div class="progress-text">
					<div class="row">
				  	<div class="col-md-6 col-sm-6 col-xs-6">
				  		Operating costs
				  	</div>
				  	<div class="col-md-6 col-sm-6 col-xs-6 text-right">
				  		60%
				  	</div>					  		
					</div>					  						  	
				</div>
				<div class="progress custom-progress">
					<div class="progress-bar primary-bar wow slideInLeft  animated" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">					      
					</div>
				</div>	
				<!-- Progress Bar End -->

				<!-- Progress Bar Start -->
				<div class="progress-text">
					<div class="row">
				  	<div class="col-md-6 col-sm-6 col-xs-6">
				  		Marketing Strategy
				  	</div>
				  	<div class="col-md-6 col-sm-6 col-xs-6 text-right">
				  		80%
				  	</div>					  		
					</div>					  						  	
				</div>
				<div class="progress custom-progress">
					<div class="progress-bar primary-bar wow slideInLeft  animated" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%;">					      
					</div>
				</div>	
				<!-- Progress Bar End -->						
			</div>			
		</div>

		<div class="row mt-100">
			<div class="col-md-5 col-sm-6 col-xs-12">
				<div class="section-heading left-holder">
					<span>First Steps</span>
					<h2>Why is it worth it?</h2>
				</div>
				<div class="text-content mt-30">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
				</div>	
				<div class="mt-20">
					<ul class="primary-list">
						<li>Aliquam fermentum lorem quis posuere mattis.</li>
						<li>Sed mollis sapien erat id pellentesque libero.</li>
						<li>Pellentesque nisl id semper bibendum.</li>
					</ul>
				</div>				
			</div>	
			<div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-1">
				<img src="http://via.placeholder.com/555x370" class="rounded-border" alt="img">
			</div>					
		</div>		
	</div>
</div>




<!-- Service Boxes START -->
<div class="section-block-grey">
	<div class="container">
		<div class="section-heading center-holder">
			<h2>Why Choose Us</h2>
			<div class="heading-line"></div>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor<br> incididunt ut labore et dolore magna aliqua. </p>
		</div>		
		<div class="row mt-40">
			<div class="col-md-4 col-sm-6 col-xs-12">
				<div class="service-box clearfix">
					<div class="box-icon">
						<i class="icon-briefcase"></i>
					</div>
					<div class="box-content">
						<h5>Design</h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer adipiscing erat eget.</p>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-6 col-xs-12">
				<div class="service-box clearfix">
					<div class="box-icon">
						<i class="icon-target"></i>
					</div>
					<div class="box-content">
						<h5>Development</h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer adipiscing erat eget.</p>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-6 col-xs-12">
				<div class="service-box clearfix">
					<div class="box-icon">
						<i class="icon-presentation"></i>
					</div>
					<div class="box-content">
						<h5>Branding</h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer adipiscing erat eget.</p>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-6 col-xs-12">
				<div class="service-box clearfix">
					<div class="box-icon">
						<i class="icon-diamond"></i>
					</div>
					<div class="box-content">
						<h5>Creative</h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer adipiscing erat eget.</p>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-6 col-xs-12">
				<div class="service-box clearfix">
					<div class="box-icon">
						<i class="icon-laptop"></i>
					</div>
					<div class="box-content">
						<h5>Business</h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer adipiscing erat eget.</p>
					</div>
				</div>
			</div>

			<div class="col-md-4 col-sm-6 col-xs-12">
				<div class="service-box clearfix">
					<div class="box-icon">
						<i class="icon-credit-card"></i>
					</div>
					<div class="box-content">
						<h5>Agency</h5>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer adipiscing erat eget.</p>
					</div>
				</div>
			</div>															
		</div>
	</div>
</div>
<!-- Service Boxes END -->




<!-- Video section START -->
<div class="section-block">
	<div class="container">
		<div class="section-heading center-holder">
			<span>First Steps</span>
			<h2>Our Intro Video</h2>
			<div class="heading-line"></div>
		</div>		
		<div class="row mt-50">
			<div class="col-md-8 col-sm-12 col-xs-12 col-md-offset-2">
				<div class="video-box">
					<img src="http://via.placeholder.com/750x422" alt="about-img" class="rounded-border">
					<div class="video-box-overlay">
						<div class="video-box-button">					
							<button class="video-play-icon" data-toggle="modal" data-target=".video-modal">
								<i class="fa fa-play"></i>
							</button>
						</div>					
						<!-- Modal Start -->
						<div class="modal fade video-modal" id="videomodal" tabindex="-1" role="dialog">
						  <div class="modal-dialog modal-lg" role="document">
							<iframe width="854" height="480" src="https://www.youtube.com/embed/Q7h81gHQXPQ"></iframe>
						  </div>
						</div>	
						<!-- Modal End -->
					</div>				
				</div>	
			</div>			
		</div>
	</div>
</div>
<!-- Video section END -->



<!-- Partners Section START -->
<div class="partner-section-grey">
	<div class="container">	
        <div class="owl-carousel owl-theme partners" id="partners">
            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">  
            </div>	

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">
            </div>

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">  
            </div>

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">  
            </div>

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">  
            </div>

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image"> 
            </div>            
        </div>  		     	
	</div>
</div>
<!-- Partners Section END -->



<?php  $this->load->view('footer');

?>