<?php  $this->load->view('header');

?>


<!-- Page Title START -->
<div class="page-title" style="background-image: url(<?php echo base_url();?>assets/img/logos/banner1.jpg); background-position: center;">
	<div class="container">
		<h1>Blog Grid</h1>
		<ul>
			<li><a href="index.html">Home</a></li>
			<li><a href="blog-grid.html">Blog Grid</a></li>
		</ul>
	</div>
</div>
<!-- Page Title END -->





<!-- Blog Gid START -->
<div class="section-block-grey">
	<div class="container">
		<div class="section-heading center-holder">
			<h2>Our News</h2>
			<div class="heading-line"></div>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor<br> incididunt ut labore et dolore magna aliqua. </p>
		</div>			
		<div class="row mt-40">
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="blog-grid">
					<div class="blog-grid-img">
						<img src="http://via.placeholder.com/358x238" alt="image">
					</div>

					<div class="blog-grid-text">
						<h4><a href="#">Plant Trees While Searching The Web</a></h4>
						<div class="blog-grid-category">
							<span>April 27, 2017 in</span><a href="#">Life Style,</a><a href="#">Photography</a>
						</div>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy </p>

						<div class="blog-grid-button">
							<a href="#">Read more <i class="fa fa-angle-right"></i></a>
						</div>
					</div>
				</div>
			</div>	

			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="blog-grid">
					<div class="blog-grid-img">
						<img src="http://via.placeholder.com/358x238" alt="image">
					</div>

					<div class="blog-grid-text">
						<h4><a href="#">Plant Trees While Searching The Web</a></h4>
						<div class="blog-grid-category">
							<span>April 27, 2017 in</span><a href="#">Life Style,</a><a href="#">Photography</a>
						</div>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy </p>

						<div class="blog-grid-button">
							<a href="#">Read more <i class="fa fa-angle-right"></i></a>
						</div>
					</div>
				</div>
			</div>			

			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="blog-grid">
					<div class="blog-grid-img">
						<img src="http://via.placeholder.com/358x238" alt="image">
					</div>

					<div class="blog-grid-text">
						<h4><a href="#">Plant Trees While Searching The Web</a></h4>
						<div class="blog-grid-category">
							<span>April 27, 2017 in</span><a href="#">Life Style,</a><a href="#">Photography</a>
						</div>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy </p>

						<div class="blog-grid-button">
							<a href="#">Read more <i class="fa fa-angle-right"></i></a>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="blog-grid">
					<div class="blog-grid-img">
						<img src="http://via.placeholder.com/358x238" alt="image">
					</div>

					<div class="blog-grid-text">
						<h4><a href="#">Plant Trees While Searching The Web</a></h4>
						<div class="blog-grid-category">
							<span>April 27, 2017 in</span><a href="#">Life Style,</a><a href="#">Photography</a>
						</div>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy </p>

						<div class="blog-grid-button">
							<a href="#">Read more <i class="fa fa-angle-right"></i></a>
						</div>
					</div>
				</div>
			</div>	

			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="blog-grid">
					<div class="blog-grid-img">
						<img src="http://via.placeholder.com/358x238" alt="image">
					</div>

					<div class="blog-grid-text">
						<h4><a href="#">Plant Trees While Searching The Web</a></h4>
						<div class="blog-grid-category">
							<span>April 27, 2017 in</span><a href="#">Life Style,</a><a href="#">Photography</a>
						</div>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy </p>

						<div class="blog-grid-button">
							<a href="#">Read more <i class="fa fa-angle-right"></i></a>
						</div>
					</div>
				</div>
			</div>	
			

			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="blog-grid">
					<div class="blog-grid-img">
						<img src="http://via.placeholder.com/358x238" alt="img">
					</div>

					<div class="blog-grid-text">
						<h4><a href="#">Plant Trees While Searching The Web</a></h4>
						<div class="blog-grid-category">
							<span>April 27, 2017 in</span><a href="#">Life Style,</a><a href="#">Photography</a>
						</div>
						<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy </p>

						<div class="blog-grid-button">
							<a href="#">Read more <i class="fa fa-angle-right"></i></a>
						</div>
					</div>
				</div>
			</div>							
		</div>
	</div>
</div>			
<!-- Blog Grid END -->



<!-- Partners Section START -->
<div class="partner-section">
	<div class="container">	
        <div class="owl-carousel owl-theme partners" id="partners">
            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">  
            </div>	

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">
            </div>

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">  
            </div>

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">  
            </div>

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">  
            </div>

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image"> 
            </div>            
        </div>  		     	
	</div>
</div>
<!-- Partners Section END -->




<?php  $this->load->view('footer');

?>