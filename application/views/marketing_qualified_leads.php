<?php  $this->load->view('header');

?>


<!-- Page Title START -->
<div class="page-title" style="background-image: url(<?php echo base_url();?>assets/img/logos/banner1.jpg); background-position: center;">
	<div class="container">
		<h1>Blog List</h1>
		<ul>
			<li><a href="index.html">Home</a></li>
			<li><a href="blog-list.html">Blog List</a></li>
		</ul>
	</div>
</div>
<!-- Page Title END -->





<!-- Blog List START -->
<div class="section-block">
	<div class="container">		
		<div class="row">
			<div class="col-md-8 col-sm-8 col-xs-12">
				<div class="blog-list">
					<div class="blog-list-img">
						 <img src="http://via.placeholder.com/750x350" alt="image">
					</div>
					<div class="blog-list-text">
						<span>21 September, 2017</span>
						<h4><a href="#">Lorem ipsum dolor sit amet, fringilsfs consectetur</a></h4>
						<p>Lorem ipsum dolor sit amet, fringilsfs consectetur adipiscing elit. Donec fringilla congue dolor, ac porttitor magna cras vel libero hendrerit vel. Linomdel Nam in. Lorem ipsum dolor sit amet, fringilsfs consectetur adipiscing elit. Donec fringilla congue dolor, ac porttitor magna cras vel libero hendrerit vel. Linomdel Nam in…</p>
						<div class="blog-grid-button">
							<a href="#">Read more <i class="fa fa-angle-right"></i></a>
						</div>
					</div>
				</div>

				<div class="blog-list">
					<div class="blog-list-img">
						 <img src="http://via.placeholder.com/750x350" alt="image">
					</div>
					<div class="blog-list-text">
						<span>21 September, 2017</span>
						<h4><a href="#">Lorem ipsum dolor sit amet, fringilsfs consectetur</a></h4>
						<p>Lorem ipsum dolor sit amet, fringilsfs consectetur adipiscing elit. Donec fringilla congue dolor, ac porttitor magna cras vel libero hendrerit vel. Linomdel Nam in. Lorem ipsum dolor sit amet, fringilsfs consectetur adipiscing elit. Donec fringilla congue dolor, ac porttitor magna cras vel libero hendrerit vel. Linomdel Nam in…</p>
						<div class="blog-grid-button">
							<a href="#">Read more <i class="fa fa-angle-right"></i></a>
						</div>
					</div>
				</div>

				<div class="blog-list">
					<div class="blog-list-img">
						 <img src="http://via.placeholder.com/750x350" alt="image">
					</div>
					<div class="blog-list-text">
						<span>21 September, 2017</span>
						<h4><a href="#">Lorem ipsum dolor sit amet, fringilsfs consectetur</a></h4>
						<p>Lorem ipsum dolor sit amet, fringilsfs consectetur adipiscing elit. Donec fringilla congue dolor, ac porttitor magna cras vel libero hendrerit vel. Linomdel Nam in. Lorem ipsum dolor sit amet, fringilsfs consectetur adipiscing elit. Donec fringilla congue dolor, ac porttitor magna cras vel libero hendrerit vel. Linomdel Nam in…</p>
						<div class="blog-grid-button">
							<a href="#">Read more <i class="fa fa-angle-right"></i></a>
						</div>
					</div>
				</div>

				<div class="blog-list">
					<div class="blog-list-img">
						 <img src="http://via.placeholder.com/750x350" alt="image">
					</div>
					<div class="blog-list-text">
						<span>21 September, 2017</span>
						<h4><a href="#">Lorem ipsum dolor sit amet, fringilsfs consectetur</a></h4>
						<p>Lorem ipsum dolor sit amet, fringilsfs consectetur adipiscing elit. Donec fringilla congue dolor, ac porttitor magna cras vel libero hendrerit vel. Linomdel Nam in. Lorem ipsum dolor sit amet, fringilsfs consectetur adipiscing elit. Donec fringilla congue dolor, ac porttitor magna cras vel libero hendrerit vel. Linomdel Nam in…</p>
						<div class="blog-grid-button">
							<a href="#">Read more <i class="fa fa-angle-right"></i></a>
						</div>
					</div>
				</div>
			</div>


			<div class="col-md-4 col-sm-4 col-xs-12">
				<div class="blog-list-right">
					<div class="blog-list-right-titles">
						<h4>Search</h4>
					</div>
					<div class="">
						<form method="get" id="search-input">
							<input type="text" name="s" placeholder="Search">
						</form>
					</div>

					<div class="blog-list-right-titles">
						<h4>Recent Posts</h4>
					</div>
					<div class="blog-list-recent">
						<div class="blog-list-recent-post">
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-4">
									<div class="blog-list-recent-img">
										<img src="http://via.placeholder.com/80x80" alt="image">
									</div>
								</div>

								<div class="col-md-8 col-sm-8 col-xs-8 pl-0 pr-0">
									<div class="blog-list-recent-text">
										 <h5><a href="#">Lorem ipsum dolor sit amet, fringilsfs consectetur</a></h5>
										 <span>21 September, 2017</span>
									</div> 
								</div>
							</div>	
						</div>

						<div class="blog-list-recent-post">
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-4">
									<div class="blog-list-recent-img">
										<img src="http://via.placeholder.com/80x80" alt="image">
									</div>
								</div>

								<div class="col-md-8 col-sm-8 col-xs-8 pl-0 pr-0">
									<div class="blog-list-recent-text">
										 <h5><a href="#">The World’s Digital Business More than Doubled in Six Years</a></h5>
										 <span>12 September, 2017</span>
									</div> 
								</div>
							</div>	
						</div>

						<div class="blog-list-recent-post">
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-4">
									<div class="blog-list-recent-img">
										<img src="http://via.placeholder.com/80x80" alt="image">
									</div>
								</div>

								<div class="col-md-8 col-sm-8 col-xs-8 pl-0 pr-0">
									<div class="blog-list-recent-text">
										 <h5><a href="#">What Marketer Can Learn from Amazon’s New Store</a></h5>
										 <span>30 September, 2017</span>
									</div> 
								</div>
							</div>	
						</div>
					</div>

					<div class="blog-list-right-titles">
						<h4>Category</h4>	
					</div>
					<div class="blog-list-category">
						<ul>
							<li><a href="#"><i class="fa fa-angle-right"></i>Blog</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i>Business Ideas</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i>Development</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i>Latest Projects</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i>News</a></li>
							<li><a href="#"><i class="fa fa-angle-right"></i>Product Design</a></li>
						</ul>
					</div>					
				</div>
			</div>							
		</div>
	</div>
</div>			
<!-- Blog List END -->


<!-- Partners Section START -->
<div class="partner-section">
	<div class="container">	
        <div class="owl-carousel owl-theme partners" id="partners">
            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">  
            </div>	

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">
            </div>

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">  
            </div>

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">  
            </div>

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image">  
            </div>

            <div class="item">
            	<img src="http://via.placeholder.com/216x108" alt="partner-image"> 
            </div>            
        </div>  		     	
	</div>
</div>
<!-- Partners Section END -->


<?php  $this->load->view('footer');

?>