<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {

	 public function __construct() 
    {
        parent::__construct();

       	// $this->load->model('employee_management_model');
        
        $this->load->helper('url');
       
        $this->load->library('session');
       
        // $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }
	public function index()
	{
		$this->load->view('services');
	}
}
