<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Demand_generation extends CI_Controller {

	 public function __construct() 
    {
        parent::__construct();

       	// $this->load->model('employee_management_model');
        
        $this->load->helper('url');
       
        $this->load->library('session');
       
        // $this->form_validation->set_error_delimiters('<div class="error">', '</div>');
    }
	public function account_based_marketing()
	{
		$this->load->view('account_based_marketing');
	}
    public function marketing_qualified_leads()
    {
        $this->load->view('marketing_qualified_leads');
    }
     public function content_marketing()
    {
        $this->load->view('content_marketing');
    }
      public function event_promotion()
    {
        $this->load->view('event_promotion');
    }
}
